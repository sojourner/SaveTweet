-- this is the file to which the selected tweet will be appended; change file name and/or path as desired
set textPathFiche to ((path to documents folder from user domain as text) & "CollectedTweets.txt")

-- tweet info: username - name - tweet - retweeted info (if exists) - date/time
-- we parse this tweet for username and tweet, discarding remaining
-- doing this results in space at end of tweet. fix.
set textRegExPattern to "([A-Za-z0-9_]+)(?: - .+? - )(.+?)(?: - Retweeted by [A-Za-z0-9_]+)?(?: - [0-9]{1,2} [A-Z]{1}[a-z]{2} [0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2})(?: )?"

-- this is the text written to the file between each tweet; revise for your preferences
set textTweetSpaceDivider to return & return & "--------" & return & return

-- text for alert button
set textButtonBon to "Bon"



-- get info from selected tweet (app only allows one selection)
tell application "System Events"
	-- if app isn't running, alert user with informational message and stop script
	if (not (exists application process "YoruFukurou")) and (not (exists application process "Night Owl")) then
		display alert "Erreur" message "This script copies the tweet selected in the Night Owl (aka YoruFukurou) app, which is not running right now. Open the app, select the tweet you want to save and run the script again." as critical buttons {textButtonBon} cancel button textButtonBon
	end if
	
	
	tell application process "YoruFukurou"
		-- bring application frontmost
		set frontmost to true
		delay 0.3
		
		-- bring front window frontmost
		keystroke "0" using command down
		delay 0.3
		
		
		tell front window
			-- find the selected row (this will be a list of one item)
			set listRowSelected to every row of table 1 of scroll area 2 whose selected is true
			
			-- get tweet info, which will be in the form of: username - name - tweet - retweeted info (if exists) - date/time 
			set textTweetInfo to value of static text 1 of item 1 of listRowSelected
		end tell
	end tell
end tell



-- capture username and tweet from tweet info, reformatting as a retweet
set textRetweet to do shell script "ruby" & space & "-e" & space & quote & "a,b=%q{" & textTweetInfo & "}" & ".match(/" & textRegExPattern & "/m)" & ".captures" & ";puts %Q{RT @#{a}: #{b}}" & quote



-- write retweet to file, ensuring file access is closed even if there's an error.
try
	set numFileRef to open for access file textPathFiche with write permission
	
	write (textRetweet & textTweetSpaceDivider) to numFileRef starting at eof as text
	
	delay 0.4
end try

close access numFileRef
