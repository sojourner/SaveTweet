#Why I Love This

Sometimes I run across fantastic tweets that I want to save and retweet later (because I've already retweeted twenty times in the last half hour). I also use this script for collecting tweets for other projects I tweet for.

This script will append the tweet selected in the Twitter client [Night Owl (aka YoruFukurou)](https://sites.google.com/site/yorufukurou/home-en) to the text file specified in the script.

#Requirements

* Twitter client [Night Owl (aka YoruFukurou)](https://sites.google.com/site/yorufukurou/home-en). 

* This script has been tested on 10.7, 10.8 and 10.10 (Lion, Mountain Lion and Yosemite). If it does not run on your version of OS X, please post a new [issue](https://github.com/seesolve/save-that-tweet/issues/new) and let me know.

#Installation

It's an AppleScript and installation is not required. You can [download the zip file](https://github.com/seeolve/save-that-tweet/archive/master.zip) and extract the script. Opening the extracted script will open AppleScript Editor (already on your Mac) and you will be able to run the script. You can also copy the raw text of this script and paste it into an AppleScript Editor window to save the script on your computer.

#Help

Post a new [issue](https://github.com/seesolve/save-that-tweet/issues/new) to request help.

#Issues

Report problems on the [issue tracking page](https://github.com/seesolve/save-that-tweet/issues/).

#Contributing

I welcome all assistance, feedback and suggestions.

#License

Read the [license](https://github.com/seesolve/save-that-tweet/blob/master/LICENSE).
